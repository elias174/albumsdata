from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute,
    NumberAttribute,
    MapAttribute,
    ListAttribute
)

class Track(MapAttribute):
    title = UnicodeAttribute()
    duration = UnicodeAttribute(null=True)

class Genre(MapAttribute):
    name = UnicodeAttribute()

class Album(Model):
    """
    A DynamoDB User
    """
    class Meta:
        table_name = 'albums'
        region = 'us-east-2'

    id = NumberAttribute(hash_key=True)
    title = UnicodeAttribute(null=True)
    year = UnicodeAttribute(null=True)
    genre = UnicodeAttribute(null=True)
    artist = UnicodeAttribute(null=True)
    tracklist = ListAttribute(of=Track)


#
# if not Album.exists():
#     Album.create_table(read_capacity_units=1, write_capacity_units=1, wait=True)
