# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from pynamodb.models import Model, LAST_EVALUATED_KEY
from pynamodb.attributes import (
    UnicodeAttribute, NumberAttribute, UnicodeSetAttribute, UTCDateTimeAttribute
)

import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.dirname( path.dirname( path.abspath(__file__) ) ) )))

from albumsdata.models import *

def foo(title__contains):
    print title__contains


def index(request):
    #album = Album.get(20264
    # albums = Album.scan(artist__contains='f', limit=20)
    # for a in albums:
    #     print a.title, a.year
    context = {}
    params = {}

    if request.method == 'GET':
        title = request.GET.get('title', '')
        if title != '':
            params['title__contains'] = str(title)
        artist = request.GET.get('artist', '')
        if artist != '':
            params['artist__contains'] = str(artist)
        year = request.GET.get('year', '')
        if year != '':
            params['year__contains'] = str(year)
        genre = request.GET.get('genre', '')
        if genre != '':
            params['genre__eq'] = str(genre)
        if len(params.values()) > 0:
            params['limit'] = 20
            albums = Album.scan(**params)
            context['albums'] = albums

    return render(request, 'searcher/index.html',context)

