from fabric.api import *
from fabric.contrib.project import rsync_project

# the user to use for the remote commands
env.user = 'eliaslinux'
# the servers where the commands are executed
env.hosts = ['192.168.0.10']


def deploy(update_from_repository=False):
    # figure out the package name and version
    with cd('/home/eliaslinux/bin/DatabaseDiscogs/albumsdata'):
        if update_from_repository:
            run('git pull origin master')
        else:
            rsync_project(local_dir='.', remote_dir='/home/eliaslinux/bin/DatabaseDiscogs/albumsdata', exclude='.git')
        run('/home/eliaslinux/InkaLabs/HVB/virtualenvs/discogs_database/bin/pip install -r requirements.txt')